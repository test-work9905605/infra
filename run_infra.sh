!/bin/bash
#source /opt/kubespray-venv/bin/activate
cd terraform
terraform init && terraform plan && terraform apply -auto-approve
cd ..
cd ansible
ansible-playbook --vault-password-file=psw.txt cluster-prepare.yaml -vvv || exit 1
if [ $? != 0 ]; then exit 1; fi
ansible-playbook --vault-password-file=psw.txt helm-charts-install.yaml -vvv
