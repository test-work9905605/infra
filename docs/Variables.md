# infra
## Ansible playbooks variables
### Encrypted variables: 
#### groups_vars/all/vault.yml:
 - prod_namespace: prod
 - agent_token
 - runner_token
#### helm-charts-install.yaml:
 - ext_host_name: ra-app.sigmanet.website
 - deploy_env: prod
#### roles/nginx-ingress/defaults/main.yml:
 - helm_chart_url: "https://kubernetes.github.io/ingress-nginx"
 - #ext_ip_list: ["some_ip"]
##### services for ingress
 - ingress_backend_staging: staging-auto-deploy 
 - ingress_backend_prod: prod-auto-deploy
#### roles/cert-manager/defaults/main.yml:
 - le_email: email for Lets Encrypt 
 - helm_chart_url: "https://charts.jetstack.io"
#### roles/kube-gitlab-runner/defaults/main.yml:
 - helm_chart_url: "https://charts.gitlab.io" 

