# Brief guide for working with the project

## Requirements
### You need to have:

#### PC or virtual machine with:
- Installed [Docker](https://docs.docker.com/engine/install)
- Git

- Active GitLab account and appropriate permissions based on your role to access the project
- Access to the AutoDeploy feature in your GitLab instance refer to ([AutoDeploy documentation](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image) for more details).
- Customization file: `.gitlab/auto-deploy-values.yml` for setting deployment parameters.


## Configuration

### GitLab

#### GitLab runners

Ensure, that group runner with tag `kube` available in the project and disable shared runners

`Group > Service > Settings > CI/CD > Runners`

#### Branches
Make sure that the main branch is protected
`<project_name> > Settings > Repository > Protected branches`

##### Settings for branches

| Branch              | Allow to merge               | Allow to push               |
|---------------------|------------------------------|-----------------------------|
| main                | Maintainers                  | No one                      |
| feature_branches    | Developers + Maintainers     | Developers + Maintainers    |


#### Project variables

`<project_name> > Settings > CI/CD > Variables`

| Key                     | Value                  | Type     | Protect | Mask | Expand reference |
|-------------------------|------------------------|----------|---------|------|------------------|
| KUBE_INGRESS_BASE_DOMAIN| Your domain name       | Variable |         | -    |                  |


#### Deployment strategy

The deployment process consists of three stages: `image build`, `staging deployment`, and `manual production deployment`.

#### Deployment Settings

To customize deployment parameters, modify the `.gitlab/auto-deploy-values.yml` file in the root folder of your project. Some common parameters include:

- `service.internalPort`: Specify the internal port for your application
- `service.externalPort`: Specify the internal port for your application  
- `priorityClassName`: priorityClass for pods (0-1000)
- `hpa`: - horizontal scaling options
- `livenessProbe`: pods liveness parameters
- `readynessProbe`: pods readyness parameters
- ...

##### GitLab agent Settings

In the project repository, you can find the GitLab Agent configuration file `config.yaml`. This file is located at `.gitlab/agents/gitlab-agent/` within the repository. The config.yaml file allows you to [customize](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#create-an-agent-configuration-file) the settings and behavior of the GitLab Agent.

##### Trigger the Deployment Pipeline

Push your changes to the GitLab repository to trigger the deployment pipeline. GitLab CI/CD will automatically execute the defined stages and jobs to build and deploy your application to the target environments.

If you encounter any issues or need further assistance, please refer to the GitLab documentation or reach out to your team for support.

## Getting Started

1. Clone the project to your PC or virtual machine using `git clone`
2. Create your own branch for development.

### Docker-compose for development

**File:** docker-compose-dev.yml

## Usage:
1. Navigate to the project's root folder
2. Run the following command:
   ```
   docker compose -f docker-compose-dev.yml up -d
   ```

# Docker-compose for production testing

**File:** docker-compose-prod.yml

## Usage:
1. Navigate to the project's root folder
2. Run the following command:
   ```
   docker compose -f docker-compose-prod.yml up -d
   ```
