## Ingress object
The ingress object is formed in the Ansible playbook using a raw manifest and the kubernetes.core.k8s module.
- File name: `ingress-app.yaml`
- Template (Jinja2): `ingress-app.yaml.j2`

#### Annotations: 
- cert-manager.io/cluster-issuer
- rewrite-target 

#### Rules:

The ingress rules specify a host, which is a DNS name pointing to the public IP address of our virtual server, where the application is accessible. 

##### Backends
The ingress manifest configuration describes two backends for two environments (staging and prod).
- pathType: Prefix 


