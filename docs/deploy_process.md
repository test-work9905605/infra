# Deployment guide for microk8s cluster:

## Requirements
### You need to have:

- Ubuntu (v20.04+) virtual machine with 12(+)Gb HDD
- Access to the virtual machine via SSH.
### PC or virtual machine with:
- Installed [Python](https://www.python.org/)
- Installed [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/index.html)

- Active Account in GitLab.com.
- GitLab folder named by your application.

---
## Deployment steps

1. Register a gitlab-runner for Kubernetes in GitLab and obtain a token for it. 
2. Additionally, it is optional to obtain a token for registering a GitLab agent. 
3. Clone the repository to your PC.
4. Navigate to the ansible project directory.
5. Configure the inventory file for ansible-playbook.
6. Modify the value of the `app_host` variable and update the `tokens` for gitlab-runner and gitlab-agent in the `ansible/group_var/all/vault.yaml file`. 
6. Run the two ansible-playbook commands sequentially with the `--ask-vault-pass` parameter, but make sure to follow this order:

- ansible-playbook -i <inventory_file> cluster-prepare.yaml --ask-vault-pass -vv
- ansible-playbook -i <inventory_file> helm-charts-install.yaml --ask-vault-pass -vv

After successfully executing the playbooks, the infrastructure is ready for use.


