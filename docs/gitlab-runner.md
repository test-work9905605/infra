## gitlab-runner

### Overview

The GitLab Runner is a crucial component in our cluster for interacting with GitLab CI/CD. 

Installation: Ansible playbook using a public [Helm chart](https://charts.gitlab.io/) and Ansible modules: 
 - kubernetes.core.helm
 - kubernetes.core.helm-repository

To configure the chart parameters, a `values.yaml` file has been created that specifies the number of replicas, connection parameters, and a link to the secret.

The runner's secret contains its token.

The secret is generated in the playbook from a Raw Manifest `secret.yaml` using the `kubernetes.core.k8s` module.
