# Project Infrastructure and Deployment Documentation

## Overview

This project is based on a series of automation tasks to set up a microk8s cluster on a virtual server. The process involves preparing the underlying infrastructure, such as VPC, EC2 instances, and security groups, and then utilizing Ansible playbooks to configure a server for the microk8s kubernetes cluster, install `microk8s`, add necessary cluster add-ons, and deploy applications using public Helm Charts and Raw Manifests.

## Table of Contents

- [Infrastructure Setup](#infrastructure-provisioning-using-terraform)
- [Ansible Playbooks](#ansible-playbooks)
- [Application Deployment with Helm](#helm-charts)
- [Raw Manifests](#raw-manifests)
- [Deployment cluster process](#deployment-cluster-process)

---
## Infrastructure Provisioning using Terraform

To create a virtual server for deploying the Microk8s cluster, Terraform is used with the AWS provider. You can find more information on how to install Terraform [here](https://www.terraform.io/downloads.html).

The infrastructure being created includes the following components:

- VPC (Virtual Private Cloud)
- EC2 instance
- Security Group (SG)
- Route53 DNS records

When creating the Terraform manifests, local modules are used. These modules are stored in a local repository.


## Ansible playbooks
You can find more information on how to install Ansible [here](https://docs.ansible.com/ansible/latest/installation_guide/index.html)

#### Overview

To deploy a Microk8s cluster, two Ansible playbooks are used:

- `cluster-prepare.yaml`
- `helm-charts-install.yaml`

The `cluster-prepare.yaml` playbook contains tasks to prepare the virtual server for cluster deployment. It includes the following actions:

1. Configuring the operating system.
2. Setting up iptables rules to allow connections only on specific ports.
3. Installing the `microk8s cluster` using snap.
4. Set up user permissions.
5. Installing cluster extensions: `rbac`, `dns`, `storage`, `metrics-server`.

The `helm-charts-install.yaml` playbook consists of three roles that utilize public Helm charts to install applications using the Ansible `kubernetes.core.helm` and `kubernetes.core.helm-repository` modules.

##### Ansible Roles:
1. cert-manager
2. kube-gitlab-runner
3. nginx-ingress

For rendering Helm manifests, `Jinja2` templates are used. `Ansible Vault` is utilized to securely store secrets.

## Helm Charts

Deploying applications with Ansible using the `kubernetes.core.helm` module allows for a streamlined and idempotent installation of Helm charts into a Kubernetes cluster, enabling a standardized approach to package deployment.

The following components are key to our Kubernetes deployment:

- **Ingress-Nginx-Controller**: This provides an HTTP and HTTPS route-based ingress controller to manage external access to services within the cluster. Repository: [here](https://kubernetes.github.io/ingress-nginx)
  
- **Cert-Manager**: This automates the management and issuance of TLS certificates to secure cluster communications. Repository: [here](https://charts.jetstack.io)

- [**GitLab Runner**](docs/gitlab-runner.md): This is used for running CI/CD pipelines defined in GitLab. A persistent runner token is configured for authentication with the GitLab instance. Repository: [here](https://charts.gitlab.io)

#### Steps to Deploy

1. Add the required Helm repositories.
2. Update the Helm repository to ensure you have the latest chart definitions.
3. Install the Helm charts for each component with the specified version and configuration options.

### Example Playbook

```yaml
- name: Install Ingress-Nginx-Controller
  hosts: k8s-cluster
  tasks:
    - name: Add Helm repository for ingress-nginx
      community.kubernetes.helm_repository:
        name: ingress-nginx
        repo_url: https://kubernetes.github.io/ingress-nginx

    - name: Install ingress-nginx Helm chart
      community.kubernetes.helm:
        name: ingress-nginx
        release_namespace: ingress-nginx
        chart_ref: ingress-nginx/ingress-nginx
        release_state: present
```

## Raw Manifests

Once foundational services are in place, additional configurations like ingresses and issuers can be deployed using Kubernetes raw manifests. The `community.kubernetes.k8s` Ansible module is used to manage these manifest files.

In the Ansible playbook `helm-charts-install.yaml`, there is also configuration for installing the following cluster objects using raw manifests:

- [Ingress](docs/ingress.md)
- Issuer
- PriorityClass

### Getting Started with Manifests

To configure `ingress` and `issuers`, you first write the YAML manifest files that define the desired state of these resources in Kubernetes. Then, use Ansible to apply these configurations to the cluster.

### Example Playbook for Manifest Configuration

```yaml
- name: Apply Ingress and Issuers configuration
  hosts: k8s-cluster
  tasks:
    - name: Configure Ingress
      kubernetes.core.k8s:
      ...
```

For each manifest file (`ingress-definition.yml` and `issuer-definition.yml`), you must construct your YAML declaration according to the configurations specific to your deployment needs.

To create an Ingress object in the cluster, the `ingress-app.yaml` manifest is used. This manifest is rendered from a Jinja2 template: `ingress-app.yaml.j2`.

To create an Issuer object in the cluster, the `ingress-issuer.yaml` manifest is used. This manifest is also rendered from a Jinja2 template: `ingress-issuer.yaml.j2`.

---
## Deployment Cluster Process
If you have a PC or virtual machine with `Ubuntu 20.04` installed along with all the necessary components (Python, Ansible), you just need to run the run_infra.sh script located in the root of this repository, after commenting out the lines related to Terraform. 
Alternatively, you can run the two ansible-playbook commands sequentially with the `--ask-vault-pass` parameter, but make sure to follow this order:

1. ansible-playbook -i <inventory_file> cluster-prepare.yaml --ask-vault-pass
2. ansible-playbook -i <inventory_file> helm-charts-install.yaml --ask-vault-pass

For more detailed information, please refer to the provided [link](docs/deploy_process.md).



###### This document serves as a brief description of the process and tools for deploying and configuring the cluster infrastructure and the microk8s cluster itself.
