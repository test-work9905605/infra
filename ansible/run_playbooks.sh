ansible-playbook --vault-password-file=psw.txt cluster-prepare.yaml -vvv
ansible-playbook --vault-password-file=psw.txt helm-charts-install.yaml -vvv