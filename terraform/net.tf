
provider "aws" {
  region = local.region
}

data "aws_availability_zones" "available" {}

locals {
  #name   = "tw_${basename(path.cwd)}"
  name   = "kube"
  region = "us-east-1"

  vpc_cidr = "10.88.0.0/16"
  azs      = slice(data.aws_availability_zones.available.names, 0, 1)  ## 1 AZ - for savings money purpose only :) 
  # azs = ["us-east-1c"]  ## for c3.large

  tags = {
    Project    = local.name
  }

  network_acls = {
    default_inbound = [ 
      {
        rule_number = 900
        rule_action = "allow"
        from_port   = 1024
        to_port     = 65535
        protocol    = "tcp"
        cidr_block  = "0.0.0.0/0"
      },
    ]
    default_outbound = [
      {
        rule_number = 900
        rule_action = "allow"
        from_port   = 0
        to_port     = 65535
        protocol    = "all"
        cidr_block  = "0.0.0.0/0"
      },
    ]
    public_inbound = [
      {
        rule_number = 90
        rule_action = "allow"
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_block  = "0.0.0.0/0"
      },
      {
        rule_number = 100
        rule_action = "allow"
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_block  = "0.0.0.0/0"
      },
      {
        rule_number = 105
        rule_action = "allow"
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_block  = "0.0.0.0/0"
      },
      {
        rule_number = 107
        rule_action = "allow"
        protocol    = "icmp"
        cidr_block  = "0.0.0.0/0"
      },
      {
        rule_number = 110
        rule_action = "allow"
        from_port   = 16443
        to_port     = 16443
        protocol    = "tcp"
        cidr_block  = "0.0.0.0/0"
      },
    ]
    public_outbound = [
      {
        rule_number = 15
        rule_action = "allow"
        protocol    = "all"
        cidr_block  = "0.0.0.0/0"
      }
    ]
    private_inbound = [
      {
        rule_number = 20
        rule_action = "allow"
        from_port   = 0
        to_port     = 65535
        protocol    = "all"
        cidr_block  = "10.88.0.0/16"
      },            
    ]
  }
}

################################################################################
# VPC Module
################################################################################

module "vpc" {
  source = "./modules/vpc"

  name = local.name
  cidr = local.vpc_cidr

  azs                 = local.azs
  #private_subnets = ["10.88.2.0/24"]
  public_subnets = ["10.88.0.0/24"]
  #private_subnets     = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k)]
  #public_subnets      = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 4)]

  public_dedicated_network_acl   = true
  public_inbound_acl_rules       = concat(local.network_acls["default_inbound"], local.network_acls["public_inbound"])
  public_outbound_acl_rules      = concat(local.network_acls["default_outbound"], local.network_acls["public_outbound"])
  # private_inbound_acl_rules       = concat(local.network_acls["default_inbound"], local.network_acls["private_inbound"])
  # private_outbound_acl_rules      = concat(local.network_acls["default_outbound"], local.network_acls["private_outbound"])

  
  # private_dedicated_network_acl     = true

  manage_default_network_acl = false

  enable_ipv6 = false

  enable_nat_gateway = false
  single_nat_gateway = true

  public_subnet_tags = {
    Name = "public_subnet"
  }
  
  # private_subnet_tags = {
  #   Name = "private_subnet"
  # }

  tags = local.tags

  vpc_tags = {
    Name = "tw_vpc"
  }
}