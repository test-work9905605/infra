# provider "aws" {
#   region = local.region
# }

# data "aws_ami" "ubuntu" {
#   owners      = ["099720109477"]
#   most_recent = true
#   filter {
#     name   = "name"
#     values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
#   }
# }

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Previously created zone in DNS
data "aws_route53_zone" "sigmanet" {
  name = "sigmanet.website"
}

locals {
#   region = "us-east-1"
  vpc_id = module.vpc.vpc_id
  zone_name = data.aws_route53_zone.sigmanet.name
  zone_id   = data.aws_route53_zone.sigmanet.zone_id
}


module "kube-servers-sg" {
  source        = "./modules/sg"
  vpc_id        = local.vpc_id
  name          = "kube-servers"
  createdby     = "Serge Sagan"
  # tcp ports
  allowed_ports = ["80", "443", "16443"]
  allowed_range = []
  # udp ports
  allowed_ports_udp = []
  allowed_range_udp = []
  cidr_blocks   = ["0.0.0.0/0"] 
}

# module "kube-server" {
#   source       = "./modules/instances"
#   ami_id       = data.aws_ami.ubuntu.id
#   vpc_security_group_ids      = [module.kube-servers-sg.security_group_id]
#   subnet_id    = module.vpc.public_subnets[0]
#   associate_public_ip_address = true
#   user_data    = file("user_data.sh")
#   srv_names    = {"kube-server-1"="10.88.0.10"}
# }

module "kube-server" {
  source         = "./modules/spot_instances"
  #instance_type  = "c3.large"
  instance_type  = "t3.medium"
  inst_max_price = 0.045
  disk_size      = 32
  ami_id         = data.aws_ami.ubuntu.id
  vpc_security_group_ids      = [module.kube-servers-sg.security_group_id]
  subnet_id      = module.vpc.public_subnets[0]
  associate_public_ip_address = true
  user_data      = file("user_data.sh")
  srv_names      = {"kube-server-1"="10.88.0.10"}
}


module "kube-rec1" {
  source    = "./modules/records"
  zone_name = local.zone_name
  zone_id   = local.zone_id
  record = [
    {
      name    = "ra-app"
      type    = "A"
      ttl     = "300"
      records = [module.kube-server.all_srv["kube-server-1"].public_ip]
    }
  ]
}


output kube_servers_private_ips  {
  value= module.kube-server.private_ip
}
